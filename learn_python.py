import os
import subprocess as sp
import argparse

PARSER = argparse.ArgumentParser()
PARSER.add_argument('--all', action='store_true')

ARGS = PARSER.parse_args()

if ARGS.all:
    try:
        from src import using_properties
        from src import using_enumerate
        from src import using_classmethod
        from src import using_staticmethod
        from src import using_decorators
    except ImportError as err:
        print('An error occurred trying to import the modules: {0}'.format(err))

else:
    learning_files = {}

    for index, learning_file in enumerate(os.listdir('src'), start=1):
        if 'using' in learning_file:
            learning_files[index] = {
                'file_name': '{0}/{1}'.format(
                    'src',
                    learning_file,
                ),
            }

    print('Select the file to run: ')
    for learning_file in learning_files:
        print('{0}.- {1}'.format(
            learning_file,
            learning_files[learning_file]['file_name']))

    try:
        selection = int(input('Option: '))
    except ValueError as err:
        print('Needs to be an integer')

    try: 
        print('Executing: {0}'.format(
            learning_files[selection]['file_name'])
        )
    except KeyError as err:
        print('Needs to be an integer between 1 and {0}'.format(
            len(learning_files))
        )

    process = sp.Popen([
            'python',
            learning_files[selection]['file_name']
        ],
        stdout=sp.PIPE,
        stderr=sp.PIPE,
    )
    out, err = process.communicate()
    if err:
        print('Error executing the script: {0}'.format(err.decode('utf-8')))
    else:
        print(out.decode('utf-8'))
