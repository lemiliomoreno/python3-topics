'''
Explain properties
'''

# 1. Let's suppose we want to create a read-only value, as an e-mail that can't
#    be changed inside a Class

class Person:

    def __init__(self, email):
        self._email = email
        
    @property
    def email(self):
        return self._email

# We create an instance of a Class
person1 = Person('my.email@mydomain.com')

# Then we invoke our getter() method
print(person1.email)

# Let's try to write on that field
try:
    person1.email = 'my.newemail@mydomain.com'
except AttributeError as err:
    print('''We can't write on this field as it doesn't have the setter() method, and it creates''')
    print('''an AttributeError exception: {0}'''.format(err))
    

# 2. Now, if we want to change that field that can be read-write, we add the
#    setter() method, we'll also add a deleter()
class Person:

    def __init__(self, email):
        self._email = email
        
    @property
    def email(self):
        return self._email
    
    @email.setter
    def email(self, new_email):
        self._email = new_email
    
    @email.deleter
    def email(self):
        del self._email
        

# We create an instance of a Class
person1 = Person('my.email@mydomain.com')

# Then we invoke our setter() method
person1.email = 'my.newemail@mydomain.com'
print(person1.email)

# And our deleter() method
del person1.email
try:
    print(person1.email)
except AttributeError as err:
    print('''We can't use it as it doesn't have it's attribute _email, we also raised an AttributeError''')
    print('''exception: {0}'''.format(err))

# When to use it?
# As docs explain: https://docs.python.org/3.8/library/functions.html#property
# This makes it possible to create read-only properties easily using property() as a decorator
