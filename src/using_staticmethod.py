
'''
Explain staticmethod() decorator
'''

# 1. If we have a class that uses a method that is not dependant from the class
#    itself, we can set it up as a @staticmethod, this method won't have access
#    to the object or the class itself. It can be used from the class itself or
#    from an instance of a class. But it won't modify anything. Let's see an
#    example.

class MyClass:
    @staticmethod
    def im_a_static_method():
        return '@staticmethod: {0}'.format('Hello!')
    
    def im_a_method(self):
        return 'method: {0}'.format(self)

# Let's use the @staticmethod, we can see it doesn't have the self or cls parameter
# so it can be used by anyone.
print(MyClass.im_a_static_method())

# Let's try to get the method:
try:
    MyClass.im_a_method()
except TypeError as err:
    print('''As is not an instance of a class, we can't access to it (nothing)''')
    print('''Raises a 'TypeError': {0}'''.format(err))

# Docs: https://docs.python.org/3/library/functions.html?highlight=classmethod#staticmethod

