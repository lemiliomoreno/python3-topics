
'''
Explain classmethod() decorator
'''

# 1. Let's take a look at @classmethod, this is a method that only inherits
#    from the class itself, no it's object, let's do an example.

class MyClass:
    @classmethod
    def im_a_classmethod(cls):
        return '@classmethod: {0}'.format(cls)

    def im_a_method(self):
        return 'method: {0}'.format(self)

# Let's use the @classmethod, we can't see is child of the class, not the object
print(MyClass.im_a_classmethod())

# Let's try to get the method:
try:
    MyClass.im_a_method()
except TypeError as err:
    print('''As is not an instance of a class, we can't access to it (nothing)''')
    print('''Raises a 'TypeError': {0}'''.format(err))

# Let's now create a instance of MyClass and use the method
method = MyClass()
print(method.im_a_method())

print('Now we see its child of the object, not the class as @classmethod')


# 2. Let's try using an __init__() function inside the class, and use the 
#    classmethod without creating an instance

class Sum:
    def __init__(self, number_one, number_two):
        self.number_one = number_one
        self.number_two = number_two

    @classmethod
    def plus_five(cls, number_one):
        return number_one + 5
    
    def __repr__(self):
        return str(self.number_one + self.number_two)

# So let's use our @classmethod to retrieve the math
print(Sum.plus_five(5))
print('''We didn't create an instance of it''')

# Now let's create an instance and retrieve the value
sum1 = Sum(1, 4)
print(sum1)

# Docs: https://docs.python.org/3/library/functions.html?highlight=classmethod#classmethod
# We know that @classmethod doesn't have access to the object itself, so it 
# can't modify it's value, but can use the method without creating an instance
# of it.
