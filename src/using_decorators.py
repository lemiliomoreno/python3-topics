# 1. Let's first see how functions work, they can be passed as parameters

def hello(person):
    return 'Hello {0}'.format(person)

def bye(person):
    return 'Bye {0}'.format(person)

# On this function, the parameter 'action', is the expected function to 
# call and return
def i_am_luis(action):
    return action('Luis')

# So here we are calling the function as the 'action' parameter
print(i_am_luis(hello))
print(i_am_luis(bye))


# 2. Let's now see how simple decorators work, passing function as parameters
def simple_decorator(func):
    def wrapper():
        print('Before execution')
        func()
        print('After execution')
    return wrapper

def action():
    print('Action!')

use_decorator = simple_decorator(action)
use_decorator()

# 3. Let's try to calculate the seconds takes a function to run
def set_timer(func):
    def wrapper():
        import time
        now = time.time()
        func()
        end = time.time()
        print('Took {0:.6} seconds to run'.format(str(end - now)))
    return wrapper
    
def count():
    n = 0
    for i in range(0, 100000):
        n += 1

use_decorator = set_timer(count)
use_decorator()

# 4. Why don't we try to do it in a more Pythonic way? Using decorators
#    we will use the outer function with @
def set_timer(func):
    def wrapper():
        import time
        now = time.time()
        func()
        end = time.time()
        print('Took {0:.8} seconds to run'.format(
            str(end - now),))
    return wrapper

@set_timer
def count():
    n = 0
    for i in range(0, 100000):
        n += 1

count()

# 5. Now, let's pass some arguments to the inner function with
#    (*args, **kwargs)
def set_timer(func):
    def wrapper(*args, **kwargs):
        import time
        now = time.time()
        func(*args, **kwargs)
        end = time.time()
        print('Took {0:.8} seconds to run'.format(str(end - now)))
    return wrapper

@set_timer
def count(max_number):
    n = 0
    for i in range(0, max_number):
        n += 1

count(100000)


# 6. To return values from the inner function, test this:
@set_timer
def count(max_number):
    n = 0
    print('Starting count: {0}'.format(n))
    for i in range(0, max_number):
        n += 1
    print('Finished count: {0}'.format(n))
    return n

try_to_return_value = count(100000)
print('Returned value: {0}'.format(try_to_return_value))

# We need to change our decorator to return a value in the inner
# function
def set_timer(func):
    def wrapper(*args, **kwargs):
        import time
        now = time.time()
        returned_value = func(*args, **kwargs)
        end = time.time()
        print('Took {0:.8} seconds to run'.format(str(end - now)))
        return returned_value
    return wrapper

@set_timer
def count(max_number):
    n = 0
    print('Starting count: {0}'.format(n))
    for i in range(0, max_number):
        n += 1
    print('Finished count: {0}'.format(n))
    return n

will_return_value = count(100000)
print('Returned value: {0}'.format(will_return_value))


# 7. We'll take a look how our decorated function properties are set
#    after decorating it

print('count() function: {0}'.format(count))
print('count.__name__: {0}'.format(count.__name__))
help('help(count): {0}'.format(help(count)))

# Apparently our count() function is pointing to our inner decorator wrapper
# on set_timer()

# Let's import a tool called 'functools'
import functools

# And now use this module to share information between them
def set_timer(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        import time
        now = time.time()
        returned_value = func(*args, **kwargs)
        end = time.time()
        print('Took {0} seconds to run'.format(str(end - now)))
        return returned_value
    return wrapper

# We didn't change anything, just decorate it again
@set_timer
def count(max_number):
    n = 0
    print('Starting count: {0}'.format(n))
    for i in range(0, max_number):
        n += 1
    print('Finished count: {0}'.format(n))
    return n

print('count() function: {0}'.format(count))
print('count.__name__: {0}'.format(count.__name__))
help('help(count): {0}'.format(help(count)))

# Now it's pointing to count()


#
# These are the basics for decorators, but as is a topic
# that I found a lot of use cases, let's continue. Wan't more?
#

# These examples were taken from: 
# https://realpython.com/primer-on-python-decorators/

# 8. Debugger:
def debug(func):
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        function_information = {
            'name': func.__name__,
            'class': func.__class__,
            'return_value': func(*args, **kwargs),
            'args': [repr(arg) for arg in args],
            'kwargs': kwargs,
        }
        # return(function_information)
        print(function_information)
    return wrapper_debug

@debug
def hello(person):
    return 'hello {0}'.format(person)

returned_value = hello('emilio')
print('Returned value: {0}'.format(returned_value))


# 9. Now let's set some decorators on classes
class TestMe:
    @debug
    def __init__(self, number_of_loops):
        self.number_of_loops = number_of_loops
    
    # On this cases, it will execute from bottom to top
    @set_timer
    @debug
    def create_loop(self):
        n = 0
        for i in range(self.number_of_loops):
            n += i ** 2

test_our_class = TestMe(10000)
test_our_class.create_loop()


# 10. Let's add parameters to our decorators
#     We need to specify the parameters, if not, we are not going to
#     be able to run it. When we declare parameters in the decorator
#     we should do this.
def set_timer(_func=None, *, times_to_run=1):
    def set_timer_decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            import time
            now = time.time()
            run_times = 0
            for times in range(times_to_run):
                func(*args, **kwargs)
                run_times += 1
            end = time.time()
            print('Took {0} seconds to run, ran {1} times'.format(
                str(end - now),
                run_times,),
            )
        return wrapper
    
    if _func is None:
        return set_timer_decorator
    else: 
        return set_timer_decorator(_func)

@set_timer(times_to_run=5)
def create_loop(number_of_loops):
    n = 0
    for i in range(number_of_loops):
        n += i ** 2

result = create_loop(
    number_of_loops = 100000,
)

# 11. State decorators
def count_calls_to_function(func):
    @functools.wraps(func)
    def wrapper_count_calls_to_function(*args, **kwargs):
        wrapper_count_calls_to_function.calls += 1
        print('Number of calls: {0}'.format(wrapper_count_calls_to_function.calls))
    wrapper_count_calls_to_function.calls = 0
    return wrapper_count_calls_to_function

@count_calls_to_function
def im_a_function():
    return True

for i in range(3):
    im_a_function()

# Docs: https://www.python.org/dev/peps/pep-0318/
