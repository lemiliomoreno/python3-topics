'''
Explain enumerate() built-in function
'''

# 1. Let's start with a simple list, suppose we wan't to get the index
#    of each of the elements in the list

months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
]

# Let's create a for loop to get the number of the month of each one
print('''It'll return a 2 value tuple():''')
for month in enumerate(months):
    print(month)

# 2. Now, let's take a list of dictionaries and get the list index of each of them
persons = [
    {
        'name': 'Luisa',
    },
    {
        'name': 'Luise',
    },
    {
        'name': 'Luisi',
    },
]

print('''It 'll return 2 values and we'll assign them on two variables''')
for index, person in enumerate(persons):
    print('index: {0}, person: {1}'.format(index, person))

# 3. Now, let's try to retrieve the index of certain letter inside a string
string = 'This is a sample string that is going to be useful on this example.'
letter_to_search = 'i'
list_of_indexes = []

for index, letter in enumerate(string):
    if letter == letter_to_search:
        list_of_indexes.append(index)

print('''These are the indexes of the letter '{0}' on the string:'''.format(letter_to_search))
print(list_of_indexes)

# Let's print the indexes of the string to see it's result
for index in list_of_indexes:
    print(string[index])

# Without enumerate() this will look like this

string = 'This is a sample string that is going to be useful on this example.'
letter_to_search = 'i'
list_of_indexes = []
index = 0

for letter in string:
    if letter == letter_to_search:
        list_of_indexes.append(index)
    index += 1

print('''These are the indexes of the letter '{0}' on the string (without enumerate):'''.format(letter_to_search))
print(list_of_indexes)

for index in list_of_indexes:
    print(string[index])

# Docs: https://docs.python.org/3.8/library/functions.html#enumerate
